package pl.ratajski.dockerfirstclient;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;



@RestController
public class Client {

    @GetMapping("/showpizzatypesstax")
    public String getStax() {

        RestTemplate restTemplate = new RestTemplate();

        ResponseEntity<String> exchange = restTemplate.exchange("http://DESKTOP-AUVTFDI:9090/pizzatypesxml",HttpMethod.GET,
                HttpEntity.EMPTY,
                String.class);

        String body = exchange.getBody();
        return StaxParser.parseFromXml(body);
    }

    @GetMapping("/showpizzatypessax")
    public String getSax() {

        RestTemplate restTemplate = new RestTemplate();

        ResponseEntity<String> exchange = restTemplate.exchange("http://DESKTOP-AUVTFDI:9090/pizzatypesxml",HttpMethod.GET,
                HttpEntity.EMPTY,
                String.class);

        String body = exchange.getBody();
        return SaxParser.parseFromXml(body);
    }

    @GetMapping("/showpizzatypesdom")
    public String getDom() {

        RestTemplate restTemplate = new RestTemplate();

        ResponseEntity<String> exchange = restTemplate.exchange("http://DESKTOP-AUVTFDI:9090/pizzatypesxml",HttpMethod.GET,
                HttpEntity.EMPTY,
                String.class);

        String body = exchange.getBody();
        return DomParser.parseFromXml(body);
    }

    @GetMapping("/showpizzatypesxml")
    public String getXml() {

        RestTemplate restTemplate = new RestTemplate();

        ResponseEntity<String> exchange = restTemplate.exchange("http://DESKTOP-AUVTFDI:9090/pizzatypesxml",HttpMethod.GET,
                HttpEntity.EMPTY,
                String.class);

        String body = exchange.getBody();
        return body;
    }

}
