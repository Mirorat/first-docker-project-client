package pl.ratajski.dockerfirstclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DockerFirstClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(DockerFirstClientApplication.class, args);
    }

}
