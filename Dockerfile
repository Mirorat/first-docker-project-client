FROM openjdk:17-ea-10-jdk
ADD build/libs/docker-first-client-0.0.1-SNAPSHOT.jar .
EXPOSE 9070
CMD java -jar docker-first-client-0.0.1-SNAPSHOT.jar